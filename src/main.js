import createSections from './join-us-section.js';

let Factory = function () {
    this.createSector = function (type) {
        let sector;
        type === 'standard' ? sector = new createSections('Join Our Program','SUBSCRIBE') :
        sector = new createSections('Join Our Advanced Program','Subscribe to Advanced program');
        
        window.addEventListener('load',sector);
}
// method for removing element
    this.remove = function(){
        const content = document.querySelector('.app-section.join_program');
        content.remove();
    }
}
    
const factory = new Factory();

// creating advanced section and removing immediately
factory.createSector('advanced');
factory.remove();

// creating standard section
factory.createSector('standard');

