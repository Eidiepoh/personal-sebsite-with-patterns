
const createSections = function(title,submit){

    const section = document.querySelector('.app-section.app-section--image-culture');
    const joinSection = document.createElement('section');
    joinSection.className = 'app-section join_program';
    
// creating section parameters
    const sectionStyle = joinSection.style;
    sectionStyle.maxWidth = '1200px';
    sectionStyle.height = '436px';
    sectionStyle.background = 'URL(assets/images/photo.png)';
    sectionStyle.color = 'white';
    sectionStyle.textAlign = 'center';

// creating innerHTML content
const content = `<h1 class="app-title">${title}</h1>
                <h2 class="app-subtitle">Sed do eiusmod tempor incididunt <br>
                ut labore et dolore magna aliqua.</h2>
                <form id="form"><input required type="email"> <button id="btn" type="submit" class="app-section__button app-section__button--read-more">${submit}</button></form>`

// inserting innerHTML content                
    joinSection.innerHTML = content;
    section.after(joinSection)

// changing styles of the inserted content    
    const input = document.querySelector('.join_program form input' );
    input.style.backgroundColor = 'rgba(0,0,0,0.15)';
    input.style.border = 'none';
    input.style.marginRight = '29px';
    input.style.width = '400px';
    input.style.height = '36px';
    input.setAttribute('placeholder','Email');
    input.style.color = 'white';

    document.querySelector('.join_program h1').style.marginTop = '87px';
  
// added event logs input text into the console
    const form = document.querySelector('#form');
    form.addEventListener('submit', (e) => {
        e.preventDefault();  
        console.log(input.value);
        input.value = '';
    });

}
export default createSections;

